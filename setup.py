"""
Installer for QDO
"""
import os
import re
import sys
from setuptools import setup, find_packages

# Synchronize version from code.
fname = os.path.join("qdo", "core.py")
version = re.findall(r"__version__ = \"(.*?)\"", open(fname).read())[0]

# Do the setup
setup(
    name="qdo",
    packages=find_packages(),
    version=version,
    # install_requires=[s.strip() for s in open("requirements.txt")],
    extras_require={},
    package_data={"qdo": ["etc/*"]},
    # package_data={"": ["*.json"]},
    author="Stephen Bailey",
    author_email="StephenBailey@lbl.gov",
    maintainer="Stephen Bailey",
    url="https://bitbucket.org/berkeleylab/qdo",
    license="BSD 3-clause",
    description="QDO",
    long_description="QDO (kew-doo) is a lightweight high-throughput queuing system designed for workflows that have many independent tasks to perform.",
    keywords=["qdo", "workflow"],
    classifiers=[
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.3",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.5",
        "Development Status :: 4 - Beta",
        "Intended Audience :: Science/Research",
        "Operating System :: OS Independent",
        "Topic :: Scientific/Engineering :: Information Analysis",
        "Topic :: Software Development :: Libraries :: Python Modules"
    ],
    scripts=['bin/qdo', 'bin/qdo-log', 'bin/qdo-pgadmin']
    #cmdclass = {
    #    "doc": BuildDocumentation,
    #},
)
