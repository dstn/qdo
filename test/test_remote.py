"""UNIT TESTS for some of the remote module within QDO


 python -m unittest test_remote
 
"""


import qdo
import qdo.remote as remote
import os
import shutil
import unittest

from getpass import getuser
from qdo import Task
from qdo.backends.postgres import AdminModule



class TestsRemote(unittest.TestCase):

    def setUp(self):
        be= os.getenv("QDO_BACKEND")
        if (not be or be=="sqlite"):
            self._db_dir="/tmp/qdo_testdb/"
            os.makedirs(self._db_dir)
            os.environ["QDO_DB_DIR"] =  self._db_dir
            self.addCleanup(self.sqlite_destroy)        
          
        elif (be=="postgres"):
            self.setup_postgres()
        else:
            raise Exception("Unsupported back-end for testing.")

        self._q=qdo.create("testq2")
    
    def sqlite_destroy(self):
        if (self._db_dir):
            shutil.rmtree(self._db_dir)
    
    def setup_postgres(self):
        self._host = os.environ.get('QDO_DB_HOST', "localhost")
        self._db_name = "unittest_db"
        self._port = os.environ.get('QDO_DB_PORT', 5432)
        self._user =  os.environ.get('QDO_DB_USER', getuser())
        self._password = os.environ.get('QDO_DB_PASS', None)
        
        os.environ["QDO_DB_HOST"]=self._host
        os.environ["QDO_DB_USER"]=getuser()
        os.environ["QDO_DB_NAME"]="unittest_db"
        
        self.addCleanup(self.db_destroy)
        self._admin = AdminModule(self._host, None, self._port,
                                  self._user, self._password)
        
        self.assertTrue(self._admin.create_new_db(self._db_name),
                        "Database creation failed.")
        
        self._db = self._admin._db
        self.addCleanup(self.user_destroy, "remote_test")
        self.assertTrue(self._admin.create_user("remote_test", "remote_test"))
        qdo.set_project("remote_test")
        
    
    def user_destroy(self, user):
        self.assertTrue(self._admin.delete_users([user]),
                       "Deleting user "+user)
    
        
    
    def db_destroy(self):
        self._admin = AdminModule(self._host, None, self._port,
                                  self._user, self._password)
        self.assertTrue(self._admin.destroy_db(self._db_name),
                        "Database destruction failed.")
        pass
            
            
    def test_add_multiple_tasks(self):
        previous_id = self._q.add("echo 0")
        
        task_list=["echo 1", "echo 2"]
        priorities=[None, 0]
        requires=[previous_id, None]
        
        new_tasks=remote.add_multiple_tasks("testq2",task_list, user=getuser(),
                                           ids=None, priorities=priorities,
                                           requires=requires)
        self.assertTrue(len(new_tasks),2)
        task1 = self._q.tasks(id=new_tasks[0]["id"])
        task2 = self._q.tasks(id=new_tasks[1]["id"])
        
        self.assertEqual(task1.task, "echo 1")
        self.assertEqual(task1.state, Task.WAITING)
        
        self.assertEqual(task2.task, "echo 2")
        self.assertEqual(task2.state, Task.PENDING)
