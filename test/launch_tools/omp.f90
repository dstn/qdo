
! MPI program that prints the ID of the CPUs executing each OpenMP thread inside
!  each MPI PE.
! Requires mycpu.c to be compiled.
! To compile:
!   icc -c mycpu.c 
!   ftn -o omp -openmp omp.f90 my_cpu.o
!
! The number of OpenMP threads per PE can be set through with the enviroment
!  variable: OMP_NUM_THREADS. e.g. export OMP_NUM_THREADS=16
!
!
program helloWorld
 implicit none
 include "mpif.h"
 integer :: myPE, numProcs, ierr, len, rc, cpu, FINDMYCPU, NTHREADS, TID, OMP_GET_NUM_THREADS, OMP_GET_THREAD_NUM, n
 INTEGER, DIMENSION (1) :: seed = (/3/) 
 REAL :: num
 character*(MPI_MAX_PROCESSOR_NAME) name
 external FINDMYCPU
 call MPI_INIT(ierr)
 call MPI_COMM_RANK(MPI_COMM_WORLD, myPE, ierr)
 call MPI_COMM_SIZE(MPI_COMM_WORLD, numProcs, ierr)
 call MPI_GET_PROCESSOR_NAME(name, len, ierr)
 if (ierr .ne. MPI_SUCCESS) then
     print *,'Error getting processor name. Terminating.'
     call MPI_ABORT(MPI_COMM_WORLD, rc, ierr)
 end if
!$OMP PARALLEL PRIVATE(NTHREADS, TID, num, seed, n, cpu) 
 num = 0
 cpu = FINDMYCPU()
 TID = OMP_GET_THREAD_NUM()
 NTHREADS = OMP_GET_NUM_THREADS()
 print *, "DATA: ", myPE," ", TID, " ", trim(name)," ", cpu, " ", num
!$OMP END PARALLEL

 call MPI_FINALIZE(ierr)
end program helloWorld
