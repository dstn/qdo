/* File: cpumodule.i SWIG description to wrap the C lib cpumodule*/
%module cpumodule

%{
#define SWIG_FILE_WITH_INIT
#include "cpumodule.h"
%}

int getmycpu();