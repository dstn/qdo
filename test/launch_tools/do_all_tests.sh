#!/bin/bash
# Runs all the tests to check if threads and processes run in different cores in the nodes.
# Ussage:
# ./do_all_tesst.sh [cores_per_node cores_per_numa]
# Arguments:
# - cores_per_node: positive integer with the number of cores per node of the system.
#   if not set, it is set to 32.
# - cores_per_numa: positive integer with the number of cores per numa in the system.
#   It not set, it is set to 16.
#
# Note: Make sure that the binaries are compiled for the current system.

NODE_CORES=32
NUMA_CORES=16
if [[ $# > 1 ]]; then
	NODE_CORES=$1
	NUMA_CORES=$2
fi
DIR=`pwd`
PYTHON_QUEUE=python_test_${NERSC_HOST}
MPI_QUEUE=mpi_test_${NERSC_HOST}
MPI_OMP_QUEUE=mpi_omp_test_${NERSC_HOST}
echo "Compiling binaries"
bash compile_omp_mpi_tools.sh 
bash compile_python_tools.sh 

echo "Testing system with ${NODE_CORES} cores per node and ${NUMA_CORES} cores per NUMA."

# First one worker, regular Python multiprocess, 32 proceesses in one node. They should be able 
# to use most of the cores.
qdo add "${PYTHON_QUEUE}" "${DIR}/cpu_test.py ${NODE_CORES}" 
qdo launch "${PYTHON_QUEUE}" 1 --cores_per_worker=${NODE_CORES} --batchqueue debug  --walltime 00:02:00 -V

# One MPI worker with 64 single threaded PEs. All should execute in different cores.
qdo add "${MPI_QUEUE}" "${DIR}/helloworld_${NERSC_HOST}" 
qdo launch "${MPI_QUEUE}" 1 --mpiproc_per_worker=${NODE_CORES} --cores_per_mpiproc=1 --walltime=0:03:00 -V \
 --verbose --batchqueue=debug

# One MPI worker with two PEs, 16 cores each. All should execute in different cores.
qdo add "${MPI_OMP_QUEUE}" "${DIR}/omp_${NERSC_HOST}" 
qdo launch "${MPI_OMP_QUEUE}" 1 --mpiproc_per_worker=2 --cores_per_mpiproc=${NUMA_CORES} --walltime=0:03:00 -V \
 --verbose --batchqueue=debug
