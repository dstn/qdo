"""UNIT TESTS for ~/.qdo/qdorc configuration mechanism.


Sample use:

 python -m unittest test_qdorc.TestQDORC
 
Gonzalo P. Rodrigo (gprodrigoalvarez@lbl.gov), 2015
"""

import os
import shutil
import sys
import unittest
import types


if sys.version_info[0] == 3:
    if sys.version_info[1] >= 4:
        from importlib import reload
    else:
        from imp import reload

class TestQDORC(unittest.TestCase):
    
    def reload_qdo(self):
        import qdo
        reload(qdo)
        import qdo.core
        import qdo.launch_calc
        reload(qdo.core)
        reload(qdo.launch_calc)
            
    def setUp(self):
        for var_name in ["QDORC_LOCATION", "QDO_DIR", "QDO_BACKEND","QDO_DB_DIR",
                         "QDO_BATCH_PROFILE", "QDO_DB_NAME","QDO_DB_HOST",
                         "QDO_DB_PORT",
                         "QDO_DB_USER", "QDO_DB_PASS"]:
            if var_name in os.environ.keys():
                del os.environ[var_name]
       
           
    def test_parse(self):
        os.environ["QDORC_LOCATION"]="/tmp/.testqdorc"
        self.dump_qdorc("/tmp/.testqdorc")
        self.reload_qdo()
        self.assertEqual(os.getenv("QDO_BACKEND"), "my_backend")
        self.assertEqual(os.getenv("QDO_DB_DIR"), "my_db_dir")
        self.assertEqual(os.getenv("QDO_BATCH_PROFILE"), "my_batch_profile")
        self.assertEqual(os.getenv("QDO_DB_NAME"), "my_db_name")
        self.assertEqual(os.getenv("QDO_DB_HOST"), "my_db_host")
        self.assertEqual(os.getenv("QDO_DB_PORT"), "my_db_port")
        self.assertEqual(os.getenv("QDO_DB_USER"), "my_db_user")
        self.assertEqual(os.getenv("QDO_DB_PASS"), "my_db_pass")
    
    def test_environ_wins(self):
        os.environ["QDO_BACKEND"] ="environ_backend"
        os.environ["QDORC_LOCATION"]="/tmp/.testqdorc"
        self.dump_qdorc("/tmp/.testqdorc")
        self.reload_qdo()    
        self.assertEqual(os.getenv("QDO_BACKEND"), "environ_backend")
    
    def test_qdo_dir_location(self):
        self.create_tmp_dir("/tmp/fake_qdir")
        os.environ["QDO_DIR"] = "/tmp/fake_qdir"
        self.dump_qdorc("/tmp/fake_qdir/qdorc", True)
        self.reload_qdo()    
        self.assertEqual(os.getenv("QDO_BACKEND"), "my_alt_backend")
        self.assertEqual(os.getenv("QDO_DB_DIR"),
                         os.path.join(os.getenv("HOME"), ".qdo", "db"))
    
    def test_home_location(self):
        old_home=os.path.expanduser("~")
        self.addCleanup(self.set_env, "HOME", old_home)

        self.create_tmp_dir("/tmp/fakehome/.qdo")
        os.environ["HOME"] = "/tmp/fakehome"
        self.dump_qdorc("/tmp/fakehome/.qdo/qdorc", True)
        self.reload_qdo()
        self.assertEqual(os.getenv("QDO_BACKEND"), "my_alt_backend")
        
    
    def test_qdorc_create(self):
        self.assertFalse(os.path.exists("/tmp/fakehome/.qdo/qdorc"))
        old_home=os.path.expanduser("~")
        self.addCleanup(self.set_env, "HOME", old_home)
        os.environ["HOME"]="/tmp/fakehome"
        self.addCleanup(self.del_fakehome)
        self.reload_qdo()
        self.assertTrue(os.path.exists("/tmp/fakehome/.qdo/qdorc"))
        
    
    def set_env(self, name, value):
        os.environ[name] = value
        
    def del_fakehome(self):
        fakehome_loc="/tmp/fakehome"
        if (os.path.exists(fakehome_loc)):
            shutil.rmtree("/tmp/fakehome")
   
    def create_tmp_dir(self, location):
        os.makedirs(location) 
        self.addCleanup(shutil.rmtree, location)
            
    def dump_qdorc(self, location, alt=False):
        qdo_rc_file=open(location, "w")
        if not alt:
            qdo_rc_file.write("""
[qdo]
qdo_backend = my_backend
qdo_db_dir = my_db_dir
qdo_dir = my_dir
qdo_batch_profile = my_batch_profile
qdo_db_name = my_db_name
qdo_db_host = my_db_host
qdo_db_port = my_db_port
qdo_db_user = my_db_user
qdo_db_pass = my_db_pass
            """)
        else:
            qdo_rc_file.write("""
[qdo]
qdo_backend = my_alt_backend
qdo_db_dir = ~/.qdo/db
            """)
        qdo_rc_file.close()
        self.addCleanup(os.remove, location)
    
    
